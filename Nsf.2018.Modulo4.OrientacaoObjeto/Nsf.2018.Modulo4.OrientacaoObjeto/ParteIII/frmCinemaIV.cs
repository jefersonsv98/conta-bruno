﻿using Nsf._2018.Modulo4.OrientacaoObjeto.ParteII;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo4.OrientacaoObjeto
{
    public partial class frmCinemaIV : Form
    {
        public frmCinemaIV()
        {
            InitializeComponent();
            cboTipoIngresso.SelectedIndex = 0;
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                string nome = txtNome.Text;
                string rg = txtRG.Text;
                decimal valor = Convert.ToDecimal(txtValor.Text);
                string Tipo1 = cboTipoIngresso.SelectedItem.ToString();

                CalcularIngresso3 calculadoraIngressos = new CalcularIngresso3();
                decimal total = calculadoraIngressos.CalculoUnitario(valor, nome, rg, Tipo1);

                lblTotal.Text = total.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lblProximo_Click(object sender, EventArgs e)
        {

        }
    }
}
