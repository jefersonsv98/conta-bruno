﻿using Nsf._2018.Modulo4.OrientacaoObjeto.ParteII;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo4.OrientacaoObjeto
{
    public partial class frmCinemaIII : Form
    {
        public frmCinemaIII()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            decimal valor = Convert.ToDecimal(txtValor.Text);
            int qtdInteira = Convert.ToInt32(txtQtdInteiras.Text);
            int qtdMeias = Convert.ToInt32(txtQtdMeias.Text);
            int qtdNiver = Convert.ToInt32(txtQtdNiver.Text);

            CalcularIngresso3 calculadoraIngressos = new CalcularIngresso3();
            decimal total = calculadoraIngressos.Calculo(valor, qtdInteira, qtdMeias, qtdNiver);

            lblTotal.Text = total.ToString();
        }

        private void lblProximo_Click(object sender, EventArgs e)
        {
            frmCinemaIV tela = new frmCinemaIV();
            tela.Show();
            Hide();
        }
    }
}
