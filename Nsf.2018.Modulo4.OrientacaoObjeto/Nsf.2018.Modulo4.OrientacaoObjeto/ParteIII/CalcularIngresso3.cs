﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo4.OrientacaoObjeto
{
    class CalcularIngresso3
    {
        public decimal Calculo (decimal valor, decimal inteira, decimal meia, decimal niver)
        {
            decimal totalInteiras = CalcularInteira (valor, inteira);
            decimal totalMeias = CalcularMeia (valor, meia);
            decimal totalNiver = CalcularNiver(valor, niver);


            decimal total = totalInteiras + totalMeias + totalNiver;
            return total;
        }

        public decimal CalcularInteira (decimal valor, decimal qtd)
        {
            decimal total = valor * qtd;

            total = Math.Round(total);

            return total;

        }

        public decimal CalcularMeia (decimal valor, decimal qtd)
        {
            decimal total = (valor / 2) * qtd;

            total = Math.Round(total);

            return total;
        }

        public decimal CalcularNiver(decimal valor, decimal qtd)
        {
            decimal total = valor * qtd * 0.3m;

            total = Math.Round(total);

            return total;
        }



        public decimal CalculoUnitario(decimal valor, string nome, string rg, string tipo)
        {
            bool valido = ValidarIngresso(nome, rg, tipo);
            if (valido == false)
                throw new Exception("RG obrigatório");


            decimal total = 0;
            if (tipo == "Inteira")
            {
                total = CalcularInteira(valor, 1);
            }

            if (tipo == "Meia")
            {
                total = CalcularMeia(valor, 1);
            }

            if (tipo == "Aniversariante")
            {
                total = CalcularNiver(valor, 1);
            }

            return total;
        }


        public bool ValidarIngresso(string nome, string RG, string nomeIngresso)
        {
            bool validado = true;

            if (nomeIngresso == "Meia" && RG == string.Empty)
            {
                validado = false;
            }

            if (nomeIngresso == "Aniversariante" && RG == string.Empty)
            {
                validado = false;
            }

            return validado;
        }
    }
}
