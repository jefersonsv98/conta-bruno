﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo4.OrientacaoObjeto.ParteII
{
    class CalculadoraIngressos
    {
        public decimal CalcularIngressos(decimal valor, int qtdInteira, int qtdMeia)
        {
            decimal totalInteiras = CalcularIngressosInteira(valor, qtdInteira);
            decimal totalMeias = CalcularIngressosMeia(valor, qtdMeia);

            decimal total = totalInteiras + totalMeias;
            return total;
        }


        public decimal CalcularIngressosInteira(decimal valor, int qtd)
        {
            decimal total = valor * qtd;

            total = Math.Round(total, 2);

            return total;
        }


        public decimal CalcularIngressosMeia(decimal valor, int qtd)
        {
            decimal total = (valor / 2) * qtd;

            total = Math.Round(total, 2);

            return total;
        }
    }
}
